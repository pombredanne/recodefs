#!/usr/bin/env python

from __future__ import with_statement

from errno import EACCES
from os.path import realpath
from sys import argv, exit
from threading import Lock
import unicodedata
import os
from collections import defaultdict
from fuse import FUSE, FuseOSError, Operations, LoggingMixIn

class MappingError(Exception):
    pass

class NameFilter(object):
    """
    Filename mapper interface for single filenames.

    Implement me for your purpose. I map between logical and physical names.
    Physical names are those on disk. Logical names are those in the virtual tree.    
    """
    description = "Identity mapping"
    def physicalToLogical(self, physName):
        "convert one filename from physical to logical"
        # default: identity mapping
        return physName
    def logicalToPhysical(self, logName):
        # default: identity mapping
        return logName

class PathNameFilter(object):
    """
    invertible mapping for paths

    This class is responsible for splitting and rejoining paths and checking
    consistency. The actual mapping is done in a NameFilter.
    """
    def __init__(self, nameFilter, debug=False):
        self.nameFilter = nameFilter
        self.debug = debug
    def physicalToLogicalPath(self, physpath):
        return '/'.join(self._physicalToLogicalFilename(x) for x in physpath.split('/'))
    def logicalToPhysicalPath(self, logpath):
        return '/'.join(self._logicalToPhysicalFilename(x) for x in logpath.split('/'))
    
    def _physicalToLogicalFilename(self, physFilename):
        logFilename = self.nameFilter.physicalToLogical(physFilename)
        if self.debug:
            physFilenameAgain = self.nameFilter.logicalToPhysical(logFilename)
            if physFilenameAgain != physFilename:
                raise MappingError("Mapping physToLogical: %r -> %r -> %r"%(physFilename, logFilename,
                                                                            physFilenameAgain))
        return logFilename
    def _logicalToPhysicalFilename(self, logFilename):
        physFilename = self.nameFilter.logicalToPhysical(logFilename)
        if self.debug:
            logFilenameAgain = self.nameFilter.physicalToLogical(physFilename)
            if logFilenameAgain != logFilename:
                raise MappingError("Mapping logicalToPhysical: %r -> %r -> %r"%(logFilename, physFilename, 
                                                                                logFilenameAgain))
        return physFilename

class DoubleEFilter(object):
    description = "stupid example: logical FS sees every 'e' doubled."
    def physicalToLogical(self, physFilename):
        return physFilename.replace('e', 'ee')
    def logicalToPhysical(self, logFilename):
        return logFilename.replace('ee', 'e')

class UnicodeNormalFormTranslator(NameFilter):
    logicalForm = None
    physicalForm = None
    def _convertToNormalForm(self, nf, u):
        return unicodedata.normalize(nf, u)
    
    def physicalToLogical(self, phys):
        return self._convertToNormalForm(self.logicalForm, phys)

    def logicalToPhysical(self, logi):
        return self._convertToNormalForm(self.physicalForm, logi)
    

class MacToOther(UnicodeNormalFormTranslator):
    description = "Present a MacOS UTF8-NFD tree in UTF8-NFC encoding for rest of the world"
    logicalForm = 'NFC'
    physicalForm = 'NFD'
        
class OtherToMac(UnicodeNormalFormTranslator):
    description = "Present a rest-of-the-world UTF8-NFC tree in UTF8-NFD for Mac"
    logicalForm = 'NFD'
    physicalForm = 'NFC'
          

def findMappingProblems(rootdir, nameFilter, fsencoding='utf8'):
    "find phys files which don't map back; iterable as filenames"
    for d,ds,fs in os.walk(rootdir):
        for f in fs+ds:
            uf = f.decode(fsencoding)
            logf = nameFilter.physicalToLogical(uf)
            uf2 = nameFilter.logicalToPhysical(logf)
            f2 = uf2.encode(fsencoding)
            if f != f2: 
                yield os.path.join(d,f)
        
def findCollisions(rootdir, nameFilter, fsencoding='utf8'):
    for d,ds,fs in os.walk(rootdir):
        collis = defaultdict(list)
        for f in fs+ds:
            uf = f.decode(fsencoding)
            logf = nameFilter.physicalToLogical(uf)
            collis[logf].append(f)
        for xs in collis.values():
            if len(xs) > 1:
                canonical=None
                others = []
                for f in xs:
                    uf = f.decode(fsencoding)
                    logf = nameFilter.physicalToLogical(uf)
                    uf2 = nameFilter.logicalToPhysical(logf)
                    f2 = uf2.encode(fsencoding)
                    if f == f2:
                        canonical = f
                    else:
                        others.append(f)
                yield(d, canonical, others)

                    
            

class NameFilterFS(LoggingMixIn, Operations):
    """
    FUSE handler which appplies a PathNameFilter to names.
    """
    def __init__(self, pathNameFilter, root):
        self.pathNameFilter = pathNameFilter
        self.root = realpath(root)
        self.rwlock = Lock()

    def __call__(self, op, path, *args):
        return super(NameFilterFS, self).__call__(op, self.root + path, *args)

    def _log2phy(self, s):
        return self.pathNameFilter.logicalToPhysicalPath(s)
    def _phy2log(self, s):
        return self.pathNameFilter.physicalToLogicalPath(s)

    def access(self, path, mode):
        if not os.access(self._log2phy(path), mode):
            raise FuseOSError(EACCES)

    def chmod(self, path, mode):
        return os.chmod(self._log2phy(path), mode)

    def chown(self, path, uid, gid):
        return os.chown(self._log2phy(path), uid, gid)

    def create(self, path, mode):
        return os.open(self._log2phy(path), os.O_WRONLY | os.O_CREAT, mode)

    def flush(self, path, fh):
        return os.fsync(fh)

    def fsync(self, path, datasync, fh):
        return os.fsync(fh)

    def getattr(self, path, fh=None):
        st = os.lstat(self._log2phy(path))
        return dict((key, getattr(st, key)) for key in ('st_atime', 'st_ctime',
            'st_gid', 'st_mode', 'st_mtime', 'st_nlink', 'st_size', 'st_uid'))

    getxattr = None

    def link(self, target, source):
        return os.link(self._log2phy(source), self._log2phy( target))

    listxattr = None

    def mkdir(self, path, *args, **kwargs):
        return os.mkdir(self._log2phy(path), *args, **kwargs)

    def mknod(self, filename, *args, **kwargs):
        return os.mknod(self._log2phy(filename), *args, **kwargs)

    def open(self, filename, *args, **kwargs):
        return os.open(self._log2phy(filename), *args, **kwargs)

    def read(self, path, size, offset, fh):
        with self.rwlock:
            os.lseek(fh, offset, 0)
            return os.read(fh, size)

    def readdir(self, path, fh):
        files = ['.', '..'] + [self._phy2log(fn) for fn in os.listdir(self._log2phy(path))]
        self._checkUnique(files)
        return files

    def _complain(self, msg):
        print msg
    def _checkUnique(self, names):
        seen = {}
        for name in names:
            if name in seen:
                self._complain("Dupe logical filename: %s")
                raise FuseOSError(EACCESS)
            seen[name] = True

    
    def readlink(self, path):
        return os.readlink(self._log2phy(path))

    def release(self, path, fh):
        return os.close(fh)

    def rename(self, old, new):
        return os.rename(self._log2phy(old), self.root + self._log2phy(new))

    def rmdir(self, dirname):
        return os.rmdir(self._log2phy(dirname))

    def statfs(self, path):
        stv = os.statvfs(self._log2phy(path))
        return dict((key, getattr(stv, key)) for key in ('f_bavail', 'f_bfree',
            'f_blocks', 'f_bsize', 'f_favail', 'f_ffree', 'f_files', 'f_flag',
            'f_frsize', 'f_namemax'))

    def symlink(self, target, source):
        return os.symlink(source, self._log2phy(target))

    def truncate(self, path, length, fh=None):
        with open(self._log2phy(path), 'r+') as f:
            f.truncate(length)

    def unlink(self, path):
        return os.unlink(self._log2phy(path))

    def utimens(self, path, *args, **kwargs):
        return os.utime(self._log2phy(path), *args, **kwargs)

    def write(self, path, data, offset, fh):
        with self.rwlock:
            os.lseek(fh, offset, 0)
            return os.write(fh, data)

def main(namefilters):
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument("rootdir", help="Root directory on disk")
    parser.add_argument("--mountpoint", "-m", default=None, help="Mount somewhere")
    parser.add_argument("--check", "-c", choices=['roundtrip', 'collisions', 'none' ],
                        default = 'none',
                        help="Check that all filenames below rootdir can be mapped back")
    parser.add_argument("-f", "--filter", choices=sorted(namefilters.keys()), required=True, help="The filter to apply")
    opts = parser.parse_args()
    nameFilter = namefilters[opts.filter]()

    allGood = True
    if opts.check == 'roundtrip':
        probs = findMappingProblems(opts.rootdir, nameFilter)
        for p in probs:
            print p
            allGood = False
    elif opts.check == 'collisions':
        probs = findCollisions(opts.rootdir, nameFilter)
        for basedir, goodp, badps in probs:
            print "%s\t%s"%(os.path.join(basedir, goodp), "\t".join(os.path.join(basedir,bp) for bp in badps))
            allGood = False
        

    if allGood and opts.mountpoint:
        pathFilter = PathNameFilter(nameFilter)
        fuse = FUSE(NameFilterFS(pathFilter, opts.rootdir), opts.mountpoint, foreground=True)
    
    
FILTERS = {
    'MacToOther' : MacToOther,
    'OtherToMac' : OtherToMac,
    'DoubleEFilter' : DoubleEFilter
}



if __name__ == '__main__':
    main(FILTERS)
